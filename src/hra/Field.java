package hra;

import javafx.scene.layout.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.scene.paint.*;


public class Field {
    private StackPane pane = new StackPane();
    private Text text;
    private Color color;
    private Rectangle square;
    private boolean isWhite;
    int x, y;

    public Field(int text, int x, int y, boolean isWhite) {
        this.x = x;
        this.y = y;
        this.isWhite = isWhite;
        square = new Rectangle(x, y, 30, 30);

        if (isWhite) {
            this.text = new Text("");
            color = Color.color(1, 1, 1);
        }
        else {
            this.text = new Text(Integer.toString(text));
            color = Color.color(.6, .7, .8);
        }

        square.setFill(color);

        pane.getChildren().addAll(square,  this.text);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public StackPane getPane() {
        return pane;
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    public String getText() {
        return text.getText();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        square.setFill(color);
    }

    public boolean isWhite() {
        return isWhite;
    }
}
