package hra;

import javafx.application.*;
import javafx.scene.control.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Hra extends Application {
    private BorderPane pane;
    private Scene scene;
    private Stage stage;
    private ArrayList<ArrayList<Field>> fields;

    private Menu mGame;
    private MenuItem miNew;
    private MenuItem miSave;
    private MenuItem miOpen;
    private MenuItem miExit;

    private int whiteRow;
    private int whiteCol;
    private int rows;
    private int cols;
    private int moves = 0;
    private boolean isStarted = false;

    private void buildMenu() {
        MenuBar menuBar = new MenuBar();
        mGame = new Menu("Game");
        miNew = new MenuItem("New");
        miSave = new MenuItem("Save");
        miOpen = new MenuItem("Open");
        miExit = new MenuItem("Exit");

        miSave.setDisable(!isStarted);
        mGame.getItems().addAll(miNew, miSave, miOpen, miExit);
        menuBar.getMenus().add(mGame);

        pane.setTop(menuBar);

        miNew.setOnAction(event -> {
            openNewGameDialog();
        });

        miSave.setOnAction(event -> {
            openSafeGameDialog();
        });

        miOpen.setOnAction(event -> {
            openLoadGameDialog();
        });

        miExit.setOnAction(event -> {
            Platform.exit();
        });

    }

    private void openNewGameDialog() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("New Game");

        TextField rows = new TextField();
        rows.setPromptText("Rows (2 - 10)");
        TextField cols = new TextField();
        cols.setPromptText("Columns (2 - 10)");
        TextField mixing = new TextField();
        mixing.setPromptText("Mixing (2 - 10 000)");
        ButtonType buttonOK = new ButtonType("OK");

        dialog.getDialogPane().getButtonTypes().setAll(buttonOK, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.add(rows, 0, 0, 1,1);
        grid.add(cols, 0, 1, 1,1);
        grid.add(mixing, 0, 2, 1,1);

        dialog.getDialogPane().setContent(grid);
        int repetition = -1;
        while (true) {
            Optional<ButtonType> result = dialog.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.CANCEL) break;
            if (result.isPresent()) {
                try {
                    this.rows = Integer.parseInt(rows.getText());
                    this.cols = Integer.parseInt(cols.getText());
                    repetition = Integer.parseInt(mixing.getText());
                    if (this.rows >= 2 && this.rows <= 20 && this.cols >= 2 && this.cols <= 20 && repetition >= 2 && repetition < 10000) {
                        break;
                    }
                } catch (NumberFormatException e) {
                    System.err.println("Unable to parse input");
                }
            }
        }
        moves = 0;
        fields = new ArrayList<>();
        isStarted = true;
        miSave.setDisable(false);
        generateFields(repetition);
        displayFields();
    }

    private void openSafeGameDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            try {
                byte[] enc = safeGame().getBytes(StandardCharsets.UTF_8);
                Files.write(Paths.get(file.getPath()), enc);
            } catch (IOException e) {
                System.err.println("Could not write to " + file);
            }
        }
    }

    private void openLoadGameDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            try {
                byte[] enc = Files.readAllBytes(Paths.get(file.getPath()));
                String game = new String(enc, StandardCharsets.UTF_8);
                loadGame(game);
            } catch (IOException e) {
                System.err.println("Could not read from " + file);
            }
        }
    }

    private void loadGame(String game) {
        Scanner s = new Scanner(game);
        try {
            fields = new ArrayList<>();
            rows = s.nextInt();
            cols = s.nextInt();
            moves = 0;
            isStarted = true;
            miSave.setDisable(false);
            for (int r = 0; r < rows; r ++) {
                ArrayList<Field> row = new ArrayList<>();
                for (int c = 0; c < cols; c++) {
                    int num = s.nextInt();
                    if (num == 0) {
                        row.add(new Field(0, r, c, true));
                        // Initialize position of white field
                        whiteRow = r;
                        whiteCol = c;
                    }
                    else {
                        row.add(new Field(num, r, c, false));
                    }
                }
                fields.add(row);
            }
            displayFields();
        }
        catch (NoSuchElementException e) {
            System.err.println("Corrupted file.");
        }
    }

    private String safeGame() {
        String game = rows + " " + cols + "\n";
        for (ArrayList<Field> row : fields) {
            for (int col = 0; col < row.size(); col++) {
                String text = row.get(col).getText();
                if (text.equals("")) text = "0";
                if (col == row.size() - 1) game += text + "\n";
                else game += text + " ";
            }
        }
        System.out.println(game);
        return game;
    }

    private ArrayList<ArrayList<Integer>> getNeighbour(int r, int c) {
        ArrayList<ArrayList<Integer>> possible = new ArrayList<>();
        if (r != 0) {
            ArrayList<Integer> field = new ArrayList<>();
            field.add(r - 1);
            field.add(c);
            possible.add(field);
        }
        if (r != rows - 1) {
            ArrayList<Integer> field = new ArrayList<>();
            field.add(r + 1);
            field.add(c);
            possible.add(field);
        }
        if (c != 0) {
            ArrayList<Integer> field = new ArrayList<>();
            field.add(r);
            field.add(c - 1);
            possible.add(field);
        }
        if (c != cols - 1) {
            ArrayList<Integer> field = new ArrayList<>();
            field.add(r);
            field.add(c + 1);
            possible.add(field);
        }
        return possible;
    }

    private void generateFields(int repetition) {
        ArrayList<ArrayList<Integer>> nums = new ArrayList<>();
        int i = 0;
        for (int r = 0; r < rows; r++) {
            ArrayList<Integer> row = new ArrayList<>();
            for (int c = 0; c < cols; c++) {
                row.add(i);
                i++;
            }
            nums.add(row);
        }

        // Randomize
        int whiteRows = rows - 1;
        int whiteCols = cols - 1;
        for (i = 0; i < repetition; i++) {
            // Neighbours [(x, y) (x, y), ...]
            ArrayList<ArrayList<Integer>> possible = getNeighbour(whiteRows, whiteCols);
            ArrayList<Integer> swap = possible.get(new Random().nextInt(possible.size()));
            int tmp = nums.get(whiteRows).get(whiteCols);
            int toSwap = nums.get(swap.get(0)).get(swap.get(1));
            nums.get(whiteRows).set(whiteCols, toSwap);
            nums.get(swap.get(0)).set(swap.get(1), tmp);
            whiteRows = swap.get(0);
            whiteCols = swap.get(1);
        }

        for (int r = 0; r < rows; r++) {
            ArrayList<Field> row = new ArrayList<>();
            for (int c = 0; c < cols; c++) {
                int num = nums.get(r).get(c);
                if (num == rows * cols - 1) {
                    row.add(new Field(0, r, c, true));
                    // Initialize position of white field
                    whiteRow = r;
                    whiteCol = c;
                }
                else {
                    row.add(new Field(num + 1, r, c, false));
                }
            }
            fields.add(row);
        }

    }

    private boolean isNextToWhite(int row, int col) {
        return ((row == whiteRow && col == whiteCol - 1) ||
                (row == whiteRow && col == whiteCol + 1) ||
                (row == whiteRow - 1 && col == whiteCol) ||
                (row == whiteRow + 1 && col == whiteCol));
    }

    private boolean isFinished() {
        int i = 1;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                if (!(Integer.toString(i).equals(fields.get(r).get(c).getText()) ||
                        (r == rows - 1 && c == cols - 1 && fields.get(r).get(c).getText().equals(""))))
                    return false;
                i++;
            }
        }
        return true;
    }

    private void displayClosingDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Congratulation!");
        alert.setContentText("Number of moves: " + moves);
        alert.setHeaderText("Congratulation!");

        ButtonType buttonOK = new ButtonType("OK");

        alert.getButtonTypes().setAll(buttonOK);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == buttonOK) {
            System.out.println("" + result.get());
            Platform.exit();
        }
    }

    private void displayFields() {
        GridPane grid = new GridPane();
        for (int r = 0; r < fields.size(); r++) {
            for (int c = 0; c < fields.get(0).size(); c++) {
                grid.add(fields.get(r).get(c).getPane(), c, r, 1, 1);
                // Add handler
                int row = fields.get(r).get(c).getX();
                int col = fields.get(r).get(c).getY();
                fields.get(r).get(c).getPane().setOnMouseClicked(event -> {
                    // System.out.println("Clicked: " + row + " " + col);
                    // System.out.println("White: " + whiteRow + " " + whiteCol);
                    Field f = fields.get(row).get(col);
                    Field whiteFiled = fields.get(whiteRow).get(whiteCol);
                    if (isNextToWhite(f.getX(), f.getY())) {
                        String tmpText = f.getText();
                        Color tmpColor = f.getColor();

                        f.setText(whiteFiled.getText());
                        f.setColor(whiteFiled.getColor());

                        whiteFiled.setText(tmpText);
                        whiteFiled.setColor(tmpColor);

                        whiteRow = f.getX();
                        whiteCol = f.getY();
                        moves++;

                        if (isFinished()) {
                            displayClosingDialog();
                        }
                    }
                });
            }
        }
        pane.setCenter(grid);
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        pane =  new BorderPane();
        scene = new Scene(pane);
        fields = new ArrayList<>();

        buildMenu();
        openNewGameDialog();

        primaryStage.setTitle("Game");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
